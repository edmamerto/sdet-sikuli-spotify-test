# Python Sikuli in BDD style

Hi! I am SDET  candidate Edgar Mamerto ([Linkedin](https://www.linkedin.com/in/edgar-mamerto-833947108/)), and this is my approach to the take home assignment, integrating BDD into Sikuli.

## Pre-requisites
This is a JAVA-free environment!

- Python 2.7
- Virtualenv (optional)
	- name your environment `venv` as this name is gitignored
- Mac OS
- Spotify Icon placed somewhere in your desktop 
- Mac's Desktop background color: `Solid Gray Pro Ultra Dark`
	- This may be necessary when finding Spotify Icon in your desktop

## Installation
```
pip install -r requirements.txt
```
Docs to libraries:
- [lackey](http://lackey.readthedocs.io/en/latest/) 
	- a pure Python Sikuli
- [Behave](https://behave.readthedocs.io/en/latest/) 
	- Python's bdd 
## Running tests

This will run all tests with  the `@all` tag in the `.feature` files. Change argument to run a specific tag
```
behave --tags=all
```
Tests are designed to run independently of other scenarios, therefore every scenario starts from a clean slate. As seen in `environment.py`.


## Features

Features are collection of test cases written in Gherkin language. You can find features in the  `/feature` directory.  

Watch them run! >>> [Screen Recording Videos](https://drive.google.com/drive/u/0/folders/1TDJSFLkaRqJHW5iOiJuw1dRAUlOdPiYa) <<<

### Feature: Login
*For a huge collection of test data, it may not be best to embed data into the script.*
```Gherkin
@login @all
Feature: Login

	Background: 
		Given I have a working internet
			And I have spotify open in my desktop

	@valid_login
	Scenario: As a Spotify free user, I can login with valid credentials 
		Given I enter my email "sdet.candidate@gmail.com"
			And I enter my password "$H!r3m3%"
		When I click the login button
		Then I should see my avatar and name in the upper right region of the view

	@invalid_login
	Scenario: As a Spotify free user, I cannot login with invalid credentials
		Given I enter my email "sdet.candidate@gmail.com"
			And I enter my password "hire%me"
		When I click the Login Button
		Then I should see the invalid login error message
```
### Feature: Search
*For a huge collection of test data, it may not be best to embed data into the script.*
```Gherkin
@search @all
Feature: Search

	Background: User is logged in
		Given I have a working internet
			And I have spotify open in my desktop
			And I enter my email "sdet.candidate@gmail.com"
			And I enter my password "$H!r3m3%"
		When I click the login button
		Then I should see my avatar and name in the upper right region of the view

	@search_popular_song
	Scenario: Popular song search
		Given I search the song "Another brick in the wall"
		Then I should see artist "Pink Floyd" in first result 

	@search_popular_artist
	Scenario: Popular artist search
		Given I search the artist "Justin"
		Then I should see artist "Justin Bieber" in first result

	@search_related_artist
	Scenario: Related artist search
		Given I search the artist "John Lennon"
			And I navigate to "John Lennon" page
		Then I should see Paul McCartney in Related Artists

	@search_popular_album
	Scenario: Popular album search
		Given I search the album "Exodus"
		Then I should see artist "Bob Marley" in first result
```
### Feature: Track
*For a huge collection of test data, it may not be best to embed data into the script.*
```Gherkin
@track @all
Feature: Track

	Background: User is logged in
		Given I have a working internet
			And I have spotify open in my desktop
			And I enter my email "sdet.candidate@gmail.com"
			And I enter my password "$H!r3m3%"
		When I click the login button
		Then I should see my avatar and name in the upper right region of the view

	@play_track
	Scenario: As a user, I can play a track
		Given I search the song "Pumped up kicks"
			And I play the song 
		Then I should see the pause button

	@pause_track
	Scenario: As a user, I can pause a track
		Given I search the song "Sleepyhead"
			And I play the song 
		Then I should see the pause button
		When I pause the song
		Then I should see the play button
```
## Locator
A locator is an element in a page. These can be constant values of `.png` files for image patttern matching, location coordinates or other ways to locate an element that Sikuli API provides.

This project approaches abstraction in a page object design manner, where page objects are abstracted by page or feature i.e. `LoginPage`, `SearchPage`, `TrackPage` .

## #5 in Assignment deliverable
"Lastly, choose any feature/functionality you want and verify that it works, document which feature you have chosen and why you chose to test this particular scenario."

### Motivation
As a premium user in a non-US country, paying non-US premium rate,  when I physically move to the US, then I should be charged at the US premium rate when I use my Spotify account.

*I chose this particular scenario because this is something I can personally relate to.*

### Precondition:
- I am a Spotify Philippine user, paying premium at Philippine rate
- I am in the Philippines

### Steps to Reproduce Issue:
1. I fly from Manila to New York
2. I live and stay in New York for 2 years
3. While in New York I enjoy the same premium account I have in the Philippines

### Expected:
I should be charged US premium rate
