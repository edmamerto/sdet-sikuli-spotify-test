'''
@author Edgar Mamerto
@date 5/27/2018
@description Spotify SDET take home assignment
'''
from lackey import *
from page.locator import Locator

def after_scenario(context, scenario):
	"""
	Always start with a clean state before scenario

	This way every scenario is independent of others
	"""
	try:
		"""
		if already logged in
		"""
		click(Locator.ACCOUNT_SETTINGS_DROPDOWN)
		click(Locator.LOGOUT_OPTION)
		click(Locator.CLOSE_WINDOW_BUTTON)
	except:
		"""
		if not yet logged in 
		"""
		click(Locator.CLOSE_WINDOW_BUTTON)