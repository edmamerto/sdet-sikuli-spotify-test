from lackey import *
from locator import Locator
from common_funcs import commonfuncs

class TrackPage(Locator):

	def __init__(self, context):
		self.context = context

	def play_song(self):
		click(self.FIRST_RESULT_LOC)

	def pause_song(self):
		click(self.PAUSE_BUTTON)

	def verify_pause_button(self):
		exists(self.PAUSE_BUTTON)

	def verify_play_button(self):
		exists(self.PLAY_BUTTON)
