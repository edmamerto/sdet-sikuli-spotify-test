import pyperclip

from lackey import *

class commonfuncs(object):

	@staticmethod
	def copy_paste(field, text):
		click(field)
		pyperclip.copy(text)
		type('v', KeyModifier.CMD)

	@staticmethod
	def clear_field(field):
		click(field)
		type('a', KeyModifier.CMD)
		type(Key.DELETE)
