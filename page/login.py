import urllib

from lackey import *
from locator import Locator
from common_funcs import commonfuncs

class LoginPage(Locator):

	_host = 'https://www.spotify.com'

	def __init__(self, context):
		self.context = context

	def check_internet_connectivity(self):
		"""
		Check if you can connect to the internet
		"""
		try: 
			urllib.urlopen(self._host)
		except:
			print 'Trouble connecting to: ' + self._host
			assert False

	def open_spotify_from_desktop(self):
		"""
		1. Double click spotify icon in dekstop
		2. Verify I see the basic login form
		"""
		doubleClick(self.SPOTIFY_DESKTOP_ICON)
		exists(self.USERNAME_EMAIL_TEXT_FIELD)
		exists(self.PASSWORD_TEXT_FIELD)
		exists(self.LOGIN_BUTTON)

	def enter_email(self, email):
		"""
		1. Click field to enable text entering
		2. Enter your email
		"""
		commonfuncs.copy_paste(self.USERNAME_EMAIL_TEXT_FIELD ,email)

	def enter_password(self, password):
		"""
		1. Click field to enable text entering
		2. Enter your password
		"""
		commonfuncs.copy_paste(self.PASSWORD_TEXT_FIELD, password)

	def click_login_button(self):
		"""
		1. Click Login button
		"""
		click(self.LOGIN_BUTTON)

	def verify_login_success(self):
		"""
		1. Verify if I see my name 
		"""
		exists(self.PROFILE_DROPDOWN_REGION)

	def verify_invalid_login_failed(self):
		"""
		1. Verify I see the error message
		2. Clear login fields for next scenario use
		"""
		exists(self.INVALID_LOGIN_ERROR_MESSAGE)
		commonfuncs.clear_field(self.USERNAME_EMAIL_TEXT_FIELD_WITH_VALUE)
		commonfuncs.clear_field(self.PASSWORD_TEXT_FIELD_WITH_VALUE)
