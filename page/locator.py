from lackey import *

class Locator(object):

  # Logout
  ACCOUNT_SETTINGS_DROPDOWN = 'account_settings_dropdown.png'
  LOGOUT_OPTION = 'logout_option.png'
  CLOSE_WINDOW_BUTTON = 'close_window_button.png'

  # Login
  SPOTIFY_DESKTOP_ICON = 'spotify_desktop_icon.png'
  PASSWORD_TEXT_FIELD = 'password_text_field.png'
  USERNAME_EMAIL_TEXT_FIELD = 'username_email_text_field.png'
  LOGIN_BUTTON = 'login_button.png'
  PROFILE_DROPDOWN_REGION = 'profile_dropdown_region.png'
  INVALID_LOGIN_ERROR_MESSAGE = 'invalid_login_error_message.png'
  USERNAME_EMAIL_TEXT_FIELD_WITH_VALUE = 'username_email_text_field_with_value.png'
  PASSWORD_TEXT_FIELD_WITH_VALUE = 'password_text_field_with_value.png'

  # Search
  SEARCH_TEXT_FIELD = 'search_text_field.png'
  BIEBER_TOP_RESULT = 'bieber_top_result.png'
  LENNON_TOP_RESULT = 'lennon_top_result.png'
  LENNON_LINK = 'lennon_link.png'
  PINK_FLOYD_TOP_RESULT = 'pink_floyd_top_result.png'
  EXODUS_ALBUM_TOP_RESULT = 'exodus_album_top_result.png'
  MCCARTNEY_RELATED_ARTIST = 'mccartney_related_artist.png'

  # Track
  FIRST_RESULT_LOC = Location(284, 211)
  PAUSE_BUTTON = 'pause_button.png'
  PLAY_BUTTON = 'play_button.png'
  







