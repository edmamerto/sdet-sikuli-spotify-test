import time

from lackey import *
from locator import Locator
from common_funcs import commonfuncs

class SearchPage(Locator):

	top_artist = {
		# Top result image and metadata 
		"Justin Bieber" : Locator.BIEBER_TOP_RESULT,
		"Pink Floyd" : Locator.PINK_FLOYD_TOP_RESULT,
		"Bob Marley" : Locator.EXODUS_ALBUM_TOP_RESULT,
		# Top result link 
		"John Lennon" : Locator.LENNON_LINK
	}

	def __init__(self, context):
		self.context = context

	def search_song(self, song):
		commonfuncs.copy_paste(self.SEARCH_TEXT_FIELD, song)
		"""
		wait 2 seconds for results to load

		Ideally we should long poll to wait 
		for some element until it loads
		"""
		time.sleep(2)

	def search_album(self, album):
		commonfuncs.copy_paste(self.SEARCH_TEXT_FIELD, album)
		"""
		wait 2 seconds for results to load
		
		Ideally we should long poll to wait 
		for some element until it loads
		"""
		time.sleep(2)

	def search_artist(self, artist):
		commonfuncs.copy_paste(self.SEARCH_TEXT_FIELD, artist)
		"""
		wait 2 seconds for results to load

		Ideally we should long poll to wait 
		for some element until it loads
		"""
		time.sleep(2)

	def verify_artist_top_result(self, artist):
		exists(self.top_artist[artist])

	def navigate_to_artist_page(self, artist):
		click(self.top_artist[artist])

	def verify_beatle_relation(self):
		exists(self.MCCARTNEY_RELATED_ARTIST)
		