@track @all
Feature: Track

	Background: User is logged in
		Given I have a working internet
			And I have spotify open in my desktop
			And I enter my email "sdet.candidate@gmail.com"
			And I enter my password "$H!r3m3%"
		When I click the login button
		Then I should see my avatar and name in the upper right region of the view

	@play_track
	Scenario: As a user, I can play a track
		Given I search the song "Pumped up kicks"
			And I play the song 
		Then I should see the pause button

	@pause_track
	Scenario: As a user, I can pause a track
		Given I search the song "Sleepyhead"
			And I play the song 
		Then I should see the pause button
		When I pause the song
		Then I should see the play button





