@search @all
Feature: Search

	Background: User is logged in
		Given I have a working internet
			And I have spotify open in my desktop
			And I enter my email "sdet.candidate@gmail.com"
			And I enter my password "$H!r3m3%"
		When I click the login button
		Then I should see my avatar and name in the upper right region of the view

	@search_popular_song
	Scenario: Popular song search
		Given I search the song "Another brick in the wall"
		Then I should see artist "Pink Floyd" in first result 

	@search_popular_artist
	Scenario: Popular artist search
		Given I search the artist "Justin"
		Then I should see artist "Justin Bieber" in first result

	@search_related_artist
	Scenario: Related artist search
		Given I search the artist "John Lennon"
			And I navigate to "John Lennon" page
		Then I should see Paul McCartney in Related Artists

	@search_popular_album
	Scenario: Popular album search
		Given I search the album "Exodus"
		Then I should see artist "Bob Marley" in first result




