@login @all
Feature: Login

	Background: 
		Given I have a working internet
			And I have spotify open in my desktop

	@valid_login
	Scenario: As a Spotify free user, I can login with valid credentials 
		Given I enter my email "sdet.candidate@gmail.com"
			And I enter my password "$H!r3m3%"
		When I click the login button
		Then I should see my avatar and name in the upper right region of the view

	@invalid_login
	Scenario: As a Spotify free user, I cannot login with invalid credentials
		Given I enter my email "sdet.candidate@gmail.com"
			And I enter my password "hire%me"
		When I click the Login Button
		Then I should see the invalid login error message




