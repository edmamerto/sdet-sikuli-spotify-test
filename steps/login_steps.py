from page.login import LoginPage

@given('I have a working internet')
def step_impl(context):
	page = LoginPage(context)
	page.check_internet_connectivity()

@given('I have spotify open in my desktop')
def step_impl(context):
    page = LoginPage(context)
    page.open_spotify_from_desktop()

@given('I enter my email "{email}"')
def step_impl(context, email):
    page = LoginPage(context)
    page.enter_email(email)

@given('I enter my password "{email}"')
def step_impl(context, email):
    page = LoginPage(context)
    page.enter_password(email)

@when('I click the login button')
def step_impl(context):
    page = LoginPage(context)
    page.click_login_button()

@then('I should see my avatar and name in the upper right region of the view')
def step_impl(context):
    page = LoginPage(context)
    page.verify_login_success()

@then('I should see the invalid login error message')
def step_impl(context):
    page = LoginPage(context)
    page.verify_invalid_login_failed()