from page.search import SearchPage

@given('I search the song "{song}"')
def step_impl(context, song):
    page = SearchPage(context)
    page.search_song(song)

@given('I search the album "{album}"')
def step_impl(context, album):
    page = SearchPage(context)
    page.search_album(album)

@given('I search the artist "{artist}"')
def step_impl(context, artist):
    page = SearchPage(context)
    page.search_artist(artist)

@then('I should see artist "{artist}" in first result')
def step_impl(context, artist):
    page = SearchPage(context)
    page.verify_artist_top_result(artist)

@given('I navigate to "{artist}" page')
def step_impl(context, artist):
    page = SearchPage(context)
    page.navigate_to_artist_page(artist)

@then('I should see Paul McCartney in Related Artists')
def step_impl(context):
    page = SearchPage(context)
    page.verify_beatle_relation()

