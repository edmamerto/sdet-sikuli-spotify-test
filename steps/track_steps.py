from page.track import TrackPage

@given('I play the song')
def step_impl(context):
    page = TrackPage(context)
    page.play_song()

@then('I should see the play button')
def step_impl(context):
    page = TrackPage(context)
    page.verify_play_button()

@then('I should see the pause button')
def step_impl(context):
    page = TrackPage(context)
    page.verify_pause_button()

@when('I pause the song')
def step_impl(context):
    page = TrackPage(context)
    page.pause_song()

